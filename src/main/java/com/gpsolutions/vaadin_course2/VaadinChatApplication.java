package com.gpsolutions.vaadin_course2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class VaadinChatApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaadinChatApplication.class, args);
	}

}

