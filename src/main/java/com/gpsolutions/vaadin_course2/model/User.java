package com.gpsolutions.vaadin_course2.model;

import com.gpsolutions.vaadin_course2.vaadin.annotations.CustomRenderer;
import com.gpsolutions.vaadin_course2.vaadin.annotations.Position;
import com.gpsolutions.vaadin_course2.vaadin.renderers.PasswordRenderer;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "USERS", uniqueConstraints = @UniqueConstraint(name = "USER_NAME_UK", columnNames = "NAME"))
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true, callSuper = true)
public class User extends AbstractNamedEntity {

    @Basic
    @Column(name = "PASSWORD")
    @Position(30)
    @CustomRenderer(PasswordRenderer.class)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE", nullable = false)
    @Position(40)
    private UserRole role;

    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "USERS_TO_ROOMS", joinColumns = {
        @JoinColumn(name = "USER_ID")}, inverseJoinColumns = {@JoinColumn(name = "ROOM_ID")})
    private List<Room> rooms;

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "sender", targetEntity = Message.class)
    private List<Message> userMessages;

    @Override
    public void beforeSave() {
        super.beforeSave();
        if (getRole() == null) {
            setRole(UserRole.USER);
        }
    }
}
