package com.gpsolutions.vaadin_course2.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "ROOMS", uniqueConstraints = @UniqueConstraint(name = "ROOMS_NAME_UK", columnNames = "NAME"))
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true, callSuper = true)
public class Room extends AbstractNamedEntity {

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "USERS_TO_ROOMS", joinColumns = {
        @JoinColumn(name = "ROOM_ID")}, inverseJoinColumns = {@JoinColumn(name = "USER_ID")})
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
