package com.gpsolutions.vaadin_course2.model;

import com.gpsolutions.vaadin_course2.vaadin.annotations.CustomRenderer;
import com.gpsolutions.vaadin_course2.vaadin.renderers.NameTextRenderer;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "MESSAGES")
@Getter
@Setter
public class Message extends AbstractEntity {

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "SENDER_ID", nullable = false)
    @CustomRenderer(NameTextRenderer.class)
    private User sender;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ROOM_ID", nullable = false)
    @CustomRenderer(NameTextRenderer.class)
    private Room room;

    @Basic
    @Column(name = "MESSAGE")
    @NotNull
    private String message;
}
