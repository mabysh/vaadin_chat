package com.gpsolutions.vaadin_course2.model;

import java.util.Arrays;

public enum UserRole {
    USER, ADMIN;

    public static String[] allRoles() {
        return Arrays.stream(UserRole.values()).map(String::valueOf).toArray(String[]::new);
    }
}
