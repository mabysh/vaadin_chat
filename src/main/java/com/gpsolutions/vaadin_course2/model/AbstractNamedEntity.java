package com.gpsolutions.vaadin_course2.model;

import com.gpsolutions.vaadin_course2.vaadin.annotations.Position;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@MappedSuperclass
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public abstract class AbstractNamedEntity extends AbstractEntity {

    @Basic
    @Column(name = "NAME")
    @Position(20)
    @ToString.Include
    private String name;
}
