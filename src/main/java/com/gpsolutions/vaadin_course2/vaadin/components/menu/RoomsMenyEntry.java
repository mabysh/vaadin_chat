package com.gpsolutions.vaadin_course2.vaadin.components.menu;

import com.gpsolutions.vaadin_course2.vaadin.views.RoomsListView;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.annotation.Secured;

@SpringComponent
@UIScope
@Order(30)
@Secured("ADMIN")
public class RoomsMenyEntry extends ButtonMenuEntry {

    public RoomsMenyEntry() {
        super("Rooms", RoomsListView.NAME);
    }
}
