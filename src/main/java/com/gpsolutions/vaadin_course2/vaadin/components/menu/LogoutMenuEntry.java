package com.gpsolutions.vaadin_course2.vaadin.components.menu;

import com.gpsolutions.vaadin_course2.vaadin.views.LogoutView;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import org.springframework.core.annotation.Order;

@SpringComponent
@UIScope
@Order(50)
public class LogoutMenuEntry extends ButtonMenuEntry {

    public LogoutMenuEntry() {
        super("Logout", LogoutView.NAME);
    }
}
