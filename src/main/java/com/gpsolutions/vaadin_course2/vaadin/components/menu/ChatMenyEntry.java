package com.gpsolutions.vaadin_course2.vaadin.components.menu;

import com.gpsolutions.vaadin_course2.vaadin.views.ChatView;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import org.springframework.core.annotation.Order;

@SpringComponent
@UIScope
@Order(10)
public class ChatMenyEntry extends ButtonMenuEntry {

    public ChatMenyEntry() {
        super("Chat", ChatView.NAME);
    }
}
