package com.gpsolutions.vaadin_course2.vaadin.components;

import com.vaadin.ui.AbstractComponent;

public interface ComponentWrapper {

    AbstractComponent getUnderlying();
}
