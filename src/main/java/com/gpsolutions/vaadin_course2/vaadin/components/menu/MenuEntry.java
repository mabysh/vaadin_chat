package com.gpsolutions.vaadin_course2.vaadin.components.menu;

import com.gpsolutions.vaadin_course2.vaadin.components.ComponentWrapper;

public interface MenuEntry extends ComponentWrapper {

    String getCaption();

    String getViewName();

    void onClick();
}
