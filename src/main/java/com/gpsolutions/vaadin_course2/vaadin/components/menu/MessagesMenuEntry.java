package com.gpsolutions.vaadin_course2.vaadin.components.menu;

import com.gpsolutions.vaadin_course2.vaadin.views.MessagesListView;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.annotation.Secured;

@SpringComponent
@UIScope
@Order(40)
@Secured("ADMIN")
public class MessagesMenuEntry extends ButtonMenuEntry {

    public MessagesMenuEntry() {
        super("Messages", MessagesListView.NAME);
    }
}
