package com.gpsolutions.vaadin_course2.vaadin.components;

import com.gpsolutions.vaadin_course2.vaadin.components.menu.MainMenu;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.themes.ValoTheme;
import javax.annotation.PostConstruct;

@UIScope
@SpringViewDisplay
public class HorizontalMainLayout extends HorizontalLayout implements MainLayout, ViewDisplay {

    private final MainMenu menu;
    private Panel viewContainer;

    public HorizontalMainLayout(final MainMenu menu) {
        this.menu = menu;
    }

    @PostConstruct
    public void init() {
        setSizeFull();
        viewContainer = new Panel();
        viewContainer.setSizeFull();
        viewContainer.addStyleName(ValoTheme.PANEL_BORDERLESS);
        addComponent(menu.getUnderlying());
        addComponentsAndExpand(viewContainer);
        setExpandRatio(menu.getUnderlying(), 1);
        setExpandRatio(viewContainer, 20);
    }

    @Override
    public AbstractComponent getUnderlying() {
        return this;
    }

    @Override
    public MainMenu getMenu() {
        return menu;
    }

    @Override
    public ViewDisplay getViewContainer() {
        return this;
    }

    @Override
    public void showView(final View view) {
        viewContainer.setContent(view.getViewComponent());
    }
}
