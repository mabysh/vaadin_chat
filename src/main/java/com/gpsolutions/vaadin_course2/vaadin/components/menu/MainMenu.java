package com.gpsolutions.vaadin_course2.vaadin.components.menu;

import com.gpsolutions.vaadin_course2.vaadin.components.ComponentWrapper;
import java.util.List;

public interface MainMenu extends ComponentWrapper {

    List<MenuEntry> menuEntries();
}
