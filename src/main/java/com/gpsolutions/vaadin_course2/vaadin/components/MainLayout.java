package com.gpsolutions.vaadin_course2.vaadin.components;

import com.gpsolutions.vaadin_course2.vaadin.components.menu.MainMenu;
import com.vaadin.navigator.ViewDisplay;

public interface MainLayout extends ComponentWrapper {

    MainMenu getMenu();

    ViewDisplay getViewContainer();
}
