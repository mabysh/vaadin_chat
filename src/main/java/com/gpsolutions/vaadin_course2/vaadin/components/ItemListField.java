package com.gpsolutions.vaadin_course2.vaadin.components;

import com.gpsolutions.vaadin_course2.model.AbstractNamedEntity;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class ItemListField<T extends AbstractNamedEntity> extends CustomField<List<T>> {

    private List<T> value;
    private VerticalLayout layout;
    private Supplier<List<T>> itemsSupplier;

    public ItemListField(final Supplier<List<T>> itemsSupplier) {
        this.itemsSupplier = itemsSupplier;
        layout = new VerticalLayout();
        layout.setMargin(false);
    }

    @Override
    protected Component initContent() {
        redraw();
        return layout;
    }

    @Override
    protected void doSetValue(final List<T> value) {
        if (value == null) {
            this.value = new ArrayList<>();
        } else {
            this.value = new ArrayList<>(value);
        }
        redraw();
    }

    private void redraw() {
        layout.removeAllComponents();
        drawExistingValues();
        drawNewValueCombobox();
    }

    private void drawExistingValues() {
        for (int i = 0; i < getValue().size(); ++i) {
            final int idx = i;
            final T item = getValue().get(i);
            final HorizontalLayout itemLayout = new HorizontalLayout();
            final Label label = new Label(item.getName());
            final Button delete = new Button();
            delete.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
            delete.setIcon(VaadinIcons.CLOSE);
            delete.addClickListener(click -> {
                getValue().remove(idx);
                redraw();
            });
            itemLayout.addComponents(label, delete);
            layout.addComponent(itemLayout);
        }
    }

    private void drawNewValueCombobox() {
        final HorizontalLayout newValueLayout = new HorizontalLayout();
        final ComboBox<T> comboBox = new ComboBox<>();
        comboBox.setDataProvider(getDataProvider());
        comboBox.setPlaceholder("Add new item...");
        comboBox.setItemCaptionGenerator(T::getName);
        final Button add = new Button();
        add.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
        add.setIcon(VaadinIcons.PLUS);
        add.setEnabled(false);
        add.addClickListener(click -> {
            getValue().add(comboBox.getValue());
            redraw();
        });
        comboBox.addValueChangeListener(valueChanged -> {
            add.setEnabled(valueChanged.getValue() != null);
        });
        newValueLayout.addComponents(comboBox, add);
        newValueLayout.setMargin(false);
        layout.addComponent(newValueLayout);
    }

    private ListDataProvider<T> getDataProvider() {
        final ListDataProvider<T> dataProvider = new ListDataProvider<>(itemsSupplier.get());
        dataProvider.addFilter(t -> {
            final String tName = t.getName();
            return getValue().stream().map(T::getName).noneMatch(tName::equals);
        });
        return dataProvider;
    }

    @Override
    public List<T> getValue() {
        return value == null ? new ArrayList<>() : value;
    }
}
