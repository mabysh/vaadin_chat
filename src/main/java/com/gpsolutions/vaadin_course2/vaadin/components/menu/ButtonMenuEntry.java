package com.gpsolutions.vaadin_course2.vaadin.components.menu;

import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

public abstract class ButtonMenuEntry extends Button implements MenuEntry {

    private String viewName;

    public ButtonMenuEntry(final String caption, final String viewName) {
        this.viewName = viewName;
        setCaption(caption);
        addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        addClickListener(click -> onClick());
    }

    @Override
    public String getCaption() {
        return super.getCaption();
    }

    @Override
    public void onClick() {
        UI.getCurrent().getNavigator().navigateTo(viewName);
    }

    @Override
    public AbstractComponent getUnderlying() {
        return this;
    }

    @Override
    public String getViewName() {
        return viewName;
    }
}
