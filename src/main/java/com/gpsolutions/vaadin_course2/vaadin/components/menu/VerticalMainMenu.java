package com.gpsolutions.vaadin_course2.vaadin.components.menu;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.CssLayout;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.security.VaadinSecurity;

@SpringComponent
@UIScope
public class VerticalMainMenu extends CssLayout implements MainMenu {

    private final int widthPixels = 100;
    private List<MenuEntry> menuEntries;
    private List<MenuEntry> accessedMenuEntries;

    @Autowired
    private VaadinSecurity security;

    @Autowired
    public VerticalMainMenu(final List<MenuEntry> menuEntries) {
        this.menuEntries = menuEntries;
        this.accessedMenuEntries = new ArrayList<>();
        setHeight(100, Unit.PERCENTAGE);
        setWidth(widthPixels, Unit.PIXELS);
        menuEntries.forEach(menu -> {
            if (security.isAuthenticated() && security.hasAccessToSecuredObject(menu)) {
                accessedMenuEntries.add(menu);
                menu.getUnderlying().setWidth(100, Unit.PERCENTAGE);
                addComponent(menu.getUnderlying());
            }
        });
    }

    @Override
    public List<MenuEntry> menuEntries() {
        return accessedMenuEntries;
    }

    @Override
    public AbstractComponent getUnderlying() {
        return this;
    }
}
