package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.Room;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;

@SpringComponent
@ViewScope
public class RoomWindowEditFormContainer extends WindowEditFormContainer<Room> {

    public RoomWindowEditFormContainer(
        final AbstractEditForm<Room> editForm) {
        super(editForm);
    }
}
