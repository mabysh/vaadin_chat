package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.Message;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.TextArea;

@SpringComponent
@ViewScope
public class MessageForm extends AbstractEditForm<Message> {

    @Override
    protected void bindEntityFields() {
        bindSender();
    }

    private void bindSender() {
        bindReadOnlyField("Sender", Message::getSender, Message::setSender);
        bindReadOnlyField("Room", Message::getRoom, Message::setRoom);
        bindMessage();
    }

    private void bindMessage() {
        final TextArea textArea = new TextArea("Message");
        getBinder().forField(textArea).bind(Message::getMessage, Message::setMessage);
        textArea.setWidth(100, Unit.PERCENTAGE);
        addComponent(textArea);
    }
}
