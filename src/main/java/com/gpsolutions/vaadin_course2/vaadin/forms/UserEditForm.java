package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.Room;
import com.gpsolutions.vaadin_course2.model.User;
import com.gpsolutions.vaadin_course2.model.UserRole;
import com.gpsolutions.vaadin_course2.service.AbstractNamedService;
import com.gpsolutions.vaadin_course2.vaadin.components.ItemListField;
import com.vaadin.data.Validator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.PasswordField;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.annotation.PrototypeScope;

@SpringComponent
@ViewScope
public class UserEditForm extends AbstractNamedEntityEditForm<User> {

    @Autowired
    private AbstractNamedService<Room> roomService;
    private ComboBox<UserRole> roleField;

    @Override
    protected void bindEntityFields() {
        super.bindEntityFields();
        bindPassword();
        bindRooms();
        bindRole();
    }

    private void bindRole() {
        roleField = new ComboBox<>();
        roleField.setCaption("Role");
        roleField.setItems(UserRole.values());
        getBinder().forField(roleField).withValidator(Validator.from(Objects::nonNull, "Set role"))
            .bind(User::getRole, User::setRole);
        addComponent(roleField);
    }

    private void bindRooms() {
        final ItemListField<Room> roomsField = new ItemListField<>(roomService::findAll);
        roomsField.setCaption("User rooms");
        getBinder().forField(roomsField).bind(User::getRooms, User::setRooms);
        addComponent(roomsField);
    }

    private void bindPassword() {
        final PasswordField passwordField = new PasswordField("Password");
        getBinder().forField(passwordField)
            .withValidator(Validator.from(StringUtils::isNotBlank, "Enter password"))
            .bind(User::getPassword, User::setPassword);
        addComponent(passwordField);
    }

    public void hideRoleField() {
        roleField.setVisible(false);
    }
}
