package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.AbstractNamedEntity;
import com.vaadin.data.Validator;
import com.vaadin.ui.TextField;
import org.apache.commons.lang3.StringUtils;

public class AbstractNamedEntityEditForm<EntityClass extends AbstractNamedEntity> extends
    AbstractEditForm<EntityClass> {

    @Override
    protected void bindEntityFields() {
        bindName();
    }

    private void bindName() {
        final TextField nameField = new TextField("Name");
        getBinder().forField(nameField)
            .withValidator(Validator.from(StringUtils::isNotBlank, "Enter name"))
            .bind(EntityClass::getName, EntityClass::setName);
        addComponent(nameField);
    }

}
