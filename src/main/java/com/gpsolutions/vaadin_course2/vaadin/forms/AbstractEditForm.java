package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.AbstractEntity;
import com.gpsolutions.vaadin_course2.model.AbstractNamedEntity;
import com.gpsolutions.vaadin_course2.service.AbstractService;
import com.vaadin.data.Binder;
import com.vaadin.data.ReadOnlyHasValue;
import com.vaadin.data.ValidationException;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractEditForm<EntityClass extends AbstractEntity> extends FormLayout {

    private Binder<EntityClass> binder;
    private EntityClass entity;
    private Runnable onSaveOrClose;

    @Autowired
    private AbstractService<EntityClass> service;
    private Button saveButton;

    public AbstractEditForm() {
        setMargin(true);
    }

    public void init(EntityClass entity) {
        this.entity = entity;
        this.binder = new Binder<>();
        this.binder.setBean(entity);
        removeAllComponents();
        initForm();
    }

    private void initForm() {
        bindID();
        bindEntityFields();
        addControls();
    }

    private void addControls() {
        final HorizontalLayout controlsLayout = new HorizontalLayout();
        saveButton = createSaveButton();
        final Button closeButton = createCloseButton();
        controlsLayout.addComponents(saveButton, closeButton);
        addComponent(controlsLayout);
    }

    private Button createCloseButton() {
        Button close = new Button("Close");
        close.addClickListener(click -> {
            getBinder().removeBean();
            if (getOnSaveOrClose() != null) {
                getOnSaveOrClose().run();
            }
        });
        return close;
    }

    private Button createSaveButton() {
        Button save = new Button("Save");
        save.addClickListener(click -> {
            try {
                final EntityClass entity = getEntity();
                getBinder().writeBean(entity);
                saveEntity(entity);
                if (getOnSaveOrClose() != null) {
                    getOnSaveOrClose().run();
                }
            } catch (ValidationException e) {
                Notification.show("Entity validation failed", Type.ERROR_MESSAGE);
            }
        });
        return save;
    }

    private void saveEntity(final EntityClass entity) {
        final EntityClass saved = getService().save(entity);
        getBinder().setBean(saved);
    }

    protected abstract void bindEntityFields();

    private void bindID() {
        bindReadOnlyField("Id", EntityClass::getId, EntityClass::setId);
    }

    protected <T> void bindReadOnlyField(final String caption,
        final ValueProvider<EntityClass, T> getter, final Setter<EntityClass, T> setter) {
        final Label label = new Label();
        label.setCaption(caption);
        ReadOnlyHasValue<T> hasValue = new ReadOnlyHasValue<>(
            t -> label.setValue(convertToString(t)));
        getBinder().forField(hasValue).bind(getter, setter);
        addComponent(label);
    }

    private <T> String convertToString(final T t) {
        if (t == null) {
            return "-";
        } else {
            if (t instanceof AbstractNamedEntity) {
                return ((AbstractNamedEntity) t).getName();
            } else {
                return String.valueOf(t);
            }
        }
    }

    public Binder<EntityClass> getBinder() {
        return binder;
    }

    public EntityClass getEntity() {
        return entity;
    }

    public AbstractService<EntityClass> getService() {
        return service;
    }

    public Runnable getOnSaveOrClose() {
        return onSaveOrClose;
    }

    public void setOnSaveOrClose(final Runnable onSaveOrClose) {
        this.onSaveOrClose = onSaveOrClose;
    }

    public Button getSaveButton() {
        return saveButton;
    }
}
