package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.Room;
import com.gpsolutions.vaadin_course2.model.User;
import com.gpsolutions.vaadin_course2.service.AbstractNamedService;
import com.gpsolutions.vaadin_course2.vaadin.components.ItemListField;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@ViewScope
public class RoomsEditForm extends AbstractNamedEntityEditForm<Room> {

    @Autowired
    private AbstractNamedService<User> userService;

    @Override
    protected void bindEntityFields() {
        super.bindEntityFields();
        bindUsers();
    }

    private void bindUsers() {
        final ItemListField<User> usersField = new ItemListField<>(userService::findAll);
        usersField.setCaption("Room users");
        getBinder().forField(usersField).bind(Room::getUsers, Room::setUsers);
        addComponent(usersField);
    }
}
