package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.Message;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;

@SpringComponent
@ViewScope
public class MessageWindowEditFormContainer extends WindowEditFormContainer<Message> {

    public MessageWindowEditFormContainer(
        final AbstractEditForm<Message> editForm) {
        super(editForm);
    }
}
