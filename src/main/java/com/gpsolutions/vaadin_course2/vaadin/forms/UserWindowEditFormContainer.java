package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.User;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;

@SpringComponent
@ViewScope
public class UserWindowEditFormContainer extends WindowEditFormContainer<User> {

    public UserWindowEditFormContainer(
        final AbstractEditForm<User> editForm) {
        super(editForm);
    }
}
