package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.User;
import com.gpsolutions.vaadin_course2.model.UserCredentials;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import java.util.function.Consumer;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@ViewScope
public class LoginForm extends FormLayout {

    private TextField usernameField;
    private PasswordField passwordField;
    private Button loginButton;
    private Button registerButton;
    private UserCredentials credentials;
    private Consumer<UserCredentials> onLogin;

    @Autowired
    private UserEditForm registrationForm;
    private Window registrationWindow;

    public LoginForm() {
        this.credentials = new UserCredentials();
    }

    @PostConstruct
    public void initContent() {
        setSizeUndefined();
        setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        usernameField = createUsernameField();
        passwordField = createPasswordField();
        loginButton = createLoginButton();
        registerButton = createRegisterButton();
        addComponents(usernameField, passwordField,
            new HorizontalLayout(loginButton, registerButton));
        setupRegistrationWindow();
    }

    private Button createLoginButton() {
        Button loginButton = new Button("Login");
        loginButton.addClickListener(click -> {
            final String username = usernameField.getValue();
            final String password = passwordField.getValue();
            if (StringUtils.isAnyBlank(username, password)) {
                Notification.show("Please provide username and password");
            } else {
                credentials.setPassword(password);
                credentials.setUsername(username);
                onLogin.accept(credentials);
            }
        });
        return loginButton;
    }

    private Button createRegisterButton() {
        final Button registerButton = new Button("Register");
        registerButton.addClickListener(click -> {
            registrationForm.init(new User());
            registrationForm.getSaveButton().setCaption("Register");
            registrationForm.hideRoleField();
            UI.getCurrent().addWindow(registrationWindow);
        });
        return registerButton;
    }

    private void setupRegistrationWindow() {
        registrationWindow = new Window("New user registration");
        registrationWindow.setClosable(false);
        registrationWindow.setModal(true);
        registrationWindow.setDraggable(false);
        registrationWindow.setResizable(false);
        registrationWindow.setWidth(80, Unit.PERCENTAGE);
        registrationWindow.setHeight(80, Unit.PERCENTAGE);
        registrationWindow.setContent(registrationForm);

        registrationForm.setOnSaveOrClose(registrationWindow::close);
    }

    private PasswordField createPasswordField() {
        return new PasswordField("Password");
    }

    private TextField createUsernameField() {
        return new TextField("Username");
    }

    public Consumer<UserCredentials> getOnLogin() {
        return onLogin;
    }

    public void setOnLogin(
        final Consumer<UserCredentials> onLogin) {
        this.onLogin = onLogin;
    }
}
