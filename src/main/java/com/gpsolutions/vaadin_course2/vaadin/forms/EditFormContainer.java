package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.AbstractEntity;

public interface EditFormContainer<EntityClass extends AbstractEntity> {

    AbstractEditForm<EntityClass> getEditForm();

    void startEdit(final EntityClass entity);

    void setOnClose(final Runnable onClose);
}
