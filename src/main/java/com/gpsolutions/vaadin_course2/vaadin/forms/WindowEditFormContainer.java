package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.AbstractEntity;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class WindowEditFormContainer<EntityClass extends AbstractEntity> extends
    Window implements
    EditFormContainer<EntityClass> {

    @Autowired
    private AbstractEditForm<EntityClass> editForm;
    private Runnable onClose;

    public WindowEditFormContainer(final AbstractEditForm<EntityClass> editForm) {
        this.editForm = editForm;
        setClosable(false);
        setModal(true);
        setDraggable(false);
        setResizable(false);
        setWidth(80, Unit.PERCENTAGE);
        setHeight(80, Unit.PERCENTAGE);
        setContent(this.editForm);
        this.editForm.setOnSaveOrClose(this::close);
    }

    @Override
    public AbstractEditForm<EntityClass> getEditForm() {
        return editForm;
    }

    @Override
    public void startEdit(final EntityClass entity) {
        getEditForm().init(entity);
        final String simpleName = entity.getClass().getSimpleName();
        if (entity.getId() == null) {
            setCaption("Add new " + simpleName);
        } else {
            setCaption("Edit " + simpleName);
        }
        UI.getCurrent().addWindow(this);
    }

    @Override
    public void setOnClose(final Runnable onClose) {
        this.onClose = onClose;
    }

    @Override
    public void detach() {
        super.detach();
        onClose.run();
    }
}
