package com.gpsolutions.vaadin_course2.vaadin.forms;

import com.gpsolutions.vaadin_course2.model.Message;
import com.gpsolutions.vaadin_course2.vaadin.util.ChatTheme;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;

@SpringComponent
@ViewScope
public class ChatForm extends VerticalLayout {

    private Supplier<List<Message>> messagesSource;
    private Consumer<String> onMessageSend;
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM HH:mm");
    private CssLayout messagesLayout;

    @PostConstruct
    public void postConstruct() {
        setSizeFull();
        setMargin(new MarginInfo(true));
        final Panel panel = new Panel();
        panel.setSizeFull();
        panel.addStyleName(ValoTheme.PANEL_BORDERLESS);
        messagesLayout = new CssLayout();
        messagesLayout.addStyleName(ChatTheme.SCROLLABLE);
        messagesLayout.setSizeFull();
        panel.setContent(messagesLayout);
        final HorizontalLayout newMessageLayout = new HorizontalLayout();
        newMessageLayout.setWidth(100, Unit.PERCENTAGE);
        newMessageLayout.setMargin(false);
        final TextField newMessageField = new TextField();
        newMessageField.setPlaceholder("Write new message...");
        newMessageField.setWidth(100, Unit.PERCENTAGE);
        final Button send = new Button("Send");
        send.setWidth(100, Unit.PERCENTAGE);
        send.addClickListener(click -> {
            final String mes = newMessageField.getValue();
            if (StringUtils.isNotBlank(mes)) {
                onMessageSend.accept(mes);
            }
            newMessageField.clear();
        });
        newMessageLayout.addComponents(newMessageField, send);
        newMessageLayout.setExpandRatio(newMessageField, 25);
        newMessageLayout.setExpandRatio(send, 2);
        addComponents(panel, newMessageLayout);
        setExpandRatio(panel, 200);
        setExpandRatio(newMessageLayout, 5);
    }

    public void redrawMessages() {
        messagesLayout.removeAllComponents();
        messagesSource.get().forEach(message -> {
            final HorizontalLayout messageLayout = new HorizontalLayout();
            messageLayout.setMargin(false);
            messageLayout.setWidth(100, Unit.PERCENTAGE);
            messageLayout.setHeightUndefined();
            final Label sender = new Label(message.getSender().getName() + ":");
            sender.addStyleName(ValoTheme.LABEL_BOLD);
            final Label msg = new Label(message.getMessage());
            msg.addStyleName(ChatTheme.WORD_WRAP);
            msg.addStyleName(ChatTheme.MARGIN_BOTTOM_15);
            final Label dateTime = new Label(message.getCreatedAt().format(dateTimeFormatter));
            messageLayout.addComponents(sender, msg, dateTime);
            messageLayout.setExpandRatio(sender, 1);
            messageLayout.setExpandRatio(dateTime, 1);
            messageLayout.setExpandRatio(msg, 10);
            messageLayout.setComponentAlignment(dateTime, Alignment.MIDDLE_RIGHT);
            messagesLayout.addComponent(messageLayout);
        });
    }

    public void setMessagesSource(final Supplier<List<Message>> messagesSource) {
        this.messagesSource = messagesSource;
        redrawMessages();
    }

    public void setOnMessageSend(final Consumer<String> onMessageSend) {
        this.onMessageSend = onMessageSend;
    }
}
