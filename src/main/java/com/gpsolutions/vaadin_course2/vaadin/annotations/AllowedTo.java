package com.gpsolutions.vaadin_course2.vaadin.annotations;

import com.gpsolutions.vaadin_course2.model.UserRole;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AllowedTo {

    UserRole[] value() default {UserRole.ADMIN, UserRole.USER};
}
