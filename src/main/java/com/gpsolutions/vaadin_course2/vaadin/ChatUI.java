package com.gpsolutions.vaadin_course2.vaadin;

import com.gpsolutions.vaadin_course2.vaadin.components.MainLayout;
import com.gpsolutions.vaadin_course2.vaadin.views.ChatView;
import com.gpsolutions.vaadin_course2.vaadin.views.ErrorView;
import com.gpsolutions.vaadin_course2.vaadin.views.LoginView;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ui.Transport;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringNavigator;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.security.VaadinSecurity;
import org.vaadin.spring.security.util.SecurityExceptionUtils;

@SpringUI
@Theme("chattheme")
@Push(transport = Transport.WEBSOCKET_XHR)
@PushStateNavigation
public class ChatUI extends UI {

    @Autowired
    private SpringNavigator navigator;
    @Autowired
    private SpringViewProvider viewProvider;
    @Autowired
    private MainLayout mainLayout;
    @Autowired
    private VaadinSecurity security;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setSizeFull();
        setContent(mainLayout.getUnderlying());
        navigator.init(this, mainLayout.getViewContainer());
        viewProvider.setAccessDeniedViewClass(ErrorView.class);
        setErrorHandler(new DefaultErrorHandler() {
            @Override
            public void error(com.vaadin.server.ErrorEvent event) {
                if (SecurityExceptionUtils.isAccessDeniedException(event.getThrowable())) {
                    Notification.show("Sorry, you don't have access to do that.");
                } else {
                    super.error(event);
                }
            }
        });
        if (security.isAuthenticated()) {
            setMenuVisibility(true);
            navigator.navigateTo(ChatView.NAME);
        } else {
            setMenuVisibility(false);
            navigator.navigateTo(LoginView.NAME);
        }
    }

    public void setMenuVisibility(final boolean visible) {
        mainLayout.getMenu().getUnderlying().setVisible(visible);
    }
}


