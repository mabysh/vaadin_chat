package com.gpsolutions.vaadin_course2.vaadin.views;

import com.gpsolutions.vaadin_course2.model.AbstractEntity;
import com.gpsolutions.vaadin_course2.service.AbstractService;
import com.gpsolutions.vaadin_course2.vaadin.forms.EditFormContainer;
import com.vaadin.data.provider.CallbackDataProvider;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class AbstractEntityListView<EntityClass extends AbstractEntity> extends
    VerticalLayout implements
    View {

    @Autowired
    private AbstractService<EntityClass> service;
    @Autowired
    private EditFormContainer<EntityClass> editFormContainer;
    @Autowired
    private Grid<EntityClass> grid;
    private Button editButton;
    private Button deleteButton;

    @Override
    public void enter(final ViewChangeEvent event) {
        setSizeFull();
        editFormContainer.setOnClose(() -> {
            grid.deselectAll();
            grid.getDataProvider().refreshAll();
        });
        addControls();
        setupGrid();
    }

    private void setupGrid() {
        setupGridDataProvider();
        grid.addSelectionListener(selection -> {
            final int selectedItemsSize = selection.getAllSelectedItems().size();
            editButton.setEnabled(selectedItemsSize == 1);
            deleteButton.setEnabled(selectedItemsSize > 0);
        });
        addComponent(grid);
        setExpandRatio(grid, 30);
    }

    protected void setupGridDataProvider() {
        CallbackDataProvider<EntityClass, String> dataProvider = new CallbackDataProvider<>(
            q -> service.findWithPagination(q),
            q -> (int) service.count());
        getGrid().setDataProvider(dataProvider);
        getGrid().getDataProvider().refreshAll();
    }

    protected abstract Class<EntityClass> getEntityClass();

    private void addControls() {
        final HorizontalLayout outerLayout = new HorizontalLayout();
        outerLayout.setWidth(100, Unit.PERCENTAGE);
        outerLayout.setMargin(false);
        final HorizontalLayout layout = new HorizontalLayout();
        layout.setMargin(false);
        outerLayout.addComponent(layout);
        final Button addButton = new Button("Add");
        addButton.addClickListener(click -> {
            try {
                editFormContainer.startEdit(getEntityClass().newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });
        deleteButton = new Button("Delete");
        deleteButton.setEnabled(false);
        deleteButton.addClickListener(click -> {
            service.deleteAll(grid.getSelectedItems());
            grid.getDataProvider().refreshAll();
        });
        editButton = new Button("Edit");
        editButton.setEnabled(false);
        editButton.addClickListener(click -> {
            final EntityClass selected = grid.getSelectedItems().iterator().next();
            editFormContainer.startEdit(selected);
        });
        layout.addComponents(addButton, editButton, deleteButton);
        addComponent(outerLayout);
        setExpandRatio(outerLayout, 1);
        addMoreControls(layout);
        showUsername(outerLayout);
    }

    private void showUsername(final HorizontalLayout layout) {
        final String name = SecurityContextHolder.getContext().getAuthentication().getName();
        Label nameLabel = new Label("Hi, " + name + "!");
        layout.addComponent(nameLabel);
        layout.setComponentAlignment(nameLabel, Alignment.MIDDLE_RIGHT);
    }

    protected void addMoreControls(final HorizontalLayout layout) {
    }

    protected Grid<EntityClass> getGrid() {
        return grid;
    }

    protected AbstractService<EntityClass> getService() {
        return service;
    }
}
