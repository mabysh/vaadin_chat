package com.gpsolutions.vaadin_course2.vaadin.views;

import com.gpsolutions.vaadin_course2.model.Room;
import com.gpsolutions.vaadin_course2.model.UserRole;
import com.gpsolutions.vaadin_course2.vaadin.ChatUI;
import com.gpsolutions.vaadin_course2.vaadin.annotations.AllowedTo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

@SpringView(name = RoomsListView.NAME, ui = ChatUI.class)
@Component(RoomsListView.NAME)
@Secured("ADMIN")
public class RoomsListView extends AbstractNamedEntityListView<Room> {

    public static final String NAME = "rooms";

    @Override
    protected Class<Room> getEntityClass() {
        return Room.class;
    }
}
