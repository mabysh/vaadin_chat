package com.gpsolutions.vaadin_course2.vaadin.views;

import com.gpsolutions.vaadin_course2.vaadin.forms.LoginForm;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.vaadin.spring.security.VaadinSecurity;

@SpringView(name = LoginView.NAME)
public class LoginView extends HorizontalLayout implements View {

    public static final String NAME = "login";

    @Autowired
    private LoginForm credentialsForm;

    @Autowired
    private VaadinSecurity vaadinSecurity;

    @Override
    public void enter(ViewChangeEvent event) {
        setSizeFull();
        setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        addComponent(credentialsForm);
        credentialsForm.setOnLogin(credentials -> {
            try {
                vaadinSecurity.login(credentials.getUsername(), credentials.getPassword());
                getUI().getPage().reload();
            } catch (AuthenticationException e) {
                Notification.show("Wrong credentials", Type.WARNING_MESSAGE);
            } catch (Exception e) {
                Notification.show("Something bad just happened...", Type.WARNING_MESSAGE);
            }
        });
    }
}
