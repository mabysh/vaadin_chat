package com.gpsolutions.vaadin_course2.vaadin.views;

import com.gpsolutions.vaadin_course2.model.Message;
import com.gpsolutions.vaadin_course2.model.Room;
import com.gpsolutions.vaadin_course2.service.BroadcastListener;
import com.gpsolutions.vaadin_course2.service.BroadcastService;
import com.gpsolutions.vaadin_course2.service.MessageService;
import com.gpsolutions.vaadin_course2.service.UserService;
import com.gpsolutions.vaadin_course2.vaadin.ChatUI;
import com.gpsolutions.vaadin_course2.vaadin.forms.ChatForm;
import com.gpsolutions.vaadin_course2.vaadin.util.ChatTheme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SpringView(name = ChatView.NAME, ui = ChatUI.class)
@Component(ChatView.NAME)
public class ChatView extends VerticalLayout implements View, BroadcastListener<Message> {

    public static final String NAME = "chat";

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private ChatForm chatForm;

    @Autowired
    private BroadcastService<Message> broadcastService;

    private UUID uuid;
    private Map<String, List<Message>> messagesByRoomName;
    private Room currentRoom;

    @Override
    public void enter(final ViewChangeEvent event) {
        uuid = UUID.randomUUID();
        setMargin(false);
        ((ChatUI) UI.getCurrent()).setMenuVisibility(true);
        final List<Room> currentUserRooms = userService.findCurrentUserRooms();
        messagesByRoomName = messageService
            .buildMessagesByRoomsMap(currentUserRooms);
        chatForm.setOnMessageSend(mes -> {
            if (currentRoom == null || StringUtils.isBlank(mes)) {
                return;
            }
            final Message newMsg = messageService.save(createNewMessage(mes));
            broadcastService.broadcast(newMsg);
        });
        broadcastService.register(this);
        initChatLayout(currentUserRooms);
    }

    @Override
    public void detach() {
        broadcastService.unregister(this);
        super.detach();
    }

    private Message createNewMessage(final String msg) {
        final Message newMessage = new Message();
        newMessage.setSender(userService.getCurrentUser());
        newMessage.setRoom(currentRoom);
        newMessage.setMessage(msg);
        return newMessage;
    }

    private void initChatLayout(final List<Room> currentUserRooms) {
        setSizeFull();
        HorizontalSplitPanel splitPanel = new HorizontalSplitPanel();
        splitPanel.addStyleName(ValoTheme.PANEL_BORDERLESS);
        splitPanel.setSplitPosition(200, Unit.PIXELS);
        splitPanel.setMinSplitPosition(200, Unit.PIXELS);
        splitPanel.setMaxSplitPosition(200, Unit.PIXELS);
        addComponentsAndExpand(splitPanel);
        splitPanel.setFirstComponent(createRoomsList(currentUserRooms));
        splitPanel.setSecondComponent(chatForm);
    }

    private VerticalLayout createRoomsList(final List<Room> currentUserRooms) {
        final VerticalLayout roomsListLayout = new VerticalLayout();
        roomsListLayout.setSizeFull();
        roomsListLayout.setMargin(false);
        final CssLayout inner = new CssLayout();
        inner.setWidth(100, Unit.PERCENTAGE);
        roomsListLayout.addComponentsAndExpand(inner);
        final List<Button> btns = new ArrayList<>();
        currentUserRooms.forEach(room -> {
            final Button btn = new Button(room.getName());
            btns.add(btn);
            btn.setWidth(100, Unit.PERCENTAGE);
            btn.addStyleName(ValoTheme.BUTTON_BORDERLESS);
            btn.addStyleName(ChatTheme.TEXT_ALIGN_LEFT);
            inner.addComponent(btn);
            btn.addClickListener(click -> {
                this.currentRoom = room;
                chatForm.setMessagesSource(() -> messagesByRoomName.get(room.getName()));
            });
        });
        if (!btns.isEmpty()) {
            btns.get(0).click();
        }
        return roomsListLayout;
    }

    @Override
    public String getIdentifier() {
        return uuid.toString();
    }

    @Override
    public void onMessage(final Message message) {
        getUI().access(() -> {
            final Room messageRoom = message.getRoom();
            messagesByRoomName.get(messageRoom.getName()).add(message);
            if (messageRoom.getId().equals(currentRoom.getId())) {
                chatForm.redrawMessages();
            }
        });
    }
}
