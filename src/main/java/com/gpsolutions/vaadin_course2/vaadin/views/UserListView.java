package com.gpsolutions.vaadin_course2.vaadin.views;

import com.gpsolutions.vaadin_course2.model.User;
import com.gpsolutions.vaadin_course2.vaadin.ChatUI;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

@SpringView(name = UserListView.NAME, ui = ChatUI.class)
@Component(UserListView.NAME)
@Secured("ADMIN")
public class UserListView extends AbstractNamedEntityListView<User> {

    public static final String NAME = "users";

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

}
