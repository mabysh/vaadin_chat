package com.gpsolutions.vaadin_course2.vaadin.views;

import com.gpsolutions.vaadin_course2.vaadin.ChatUI;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.HorizontalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.security.VaadinSecurity;

@SpringView(name = LogoutView.NAME, ui = ChatUI.class)
@Component(LogoutView.NAME)
public class LogoutView extends HorizontalLayout implements View {

    public static final String NAME = "logout";

    @Autowired
    private VaadinSecurity security;

    @Override
    public void enter(ViewChangeEvent event) {
        security.logout();
    }
}
