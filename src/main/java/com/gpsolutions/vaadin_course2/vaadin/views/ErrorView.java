package com.gpsolutions.vaadin_course2.vaadin.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

@Component(ErrorView.NAME)
@PrototypeScope
public class ErrorView extends VerticalLayout implements View {

    public static final String NAME = "error";

    @Override
    public void enter(ViewChangeEvent event) {
        addComponent(new Label("NO ACCESS"));
    }
}
