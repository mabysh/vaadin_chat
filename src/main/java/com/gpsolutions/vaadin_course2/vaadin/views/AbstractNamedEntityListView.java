package com.gpsolutions.vaadin_course2.vaadin.views;

import com.gpsolutions.vaadin_course2.model.AbstractNamedEntity;
import com.gpsolutions.vaadin_course2.service.AbstractNamedService;
import com.vaadin.data.provider.CallbackDataProvider;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;

public abstract class AbstractNamedEntityListView<EntityClass extends AbstractNamedEntity> extends
    AbstractEntityListView<EntityClass> {

    @Override
    protected void setupGridDataProvider() {
        getGrid().setDataProvider(new CallbackDataProvider<EntityClass, String>(
            q -> getService().findFilteredByNameWithPagination(q),
            q -> (int) getService().findFilteredByNameWithPagination(q).count())
            .withConfigurableFilter());
        getGrid().getDataProvider().refreshAll();
    }

    @Override
    public AbstractNamedService<EntityClass> getService() {
        return (AbstractNamedService<EntityClass>) super.getService();
    }

    @Override
    protected void addMoreControls(final HorizontalLayout layout) {
        final TextField filterField = setupFilterField();
        layout.addComponent(filterField);
    }

    private TextField setupFilterField() {
        final TextField tf = new TextField();
        tf.setPlaceholder("Filter by name...");
        tf.addValueChangeListener(valueChanged -> {
            ((ConfigurableFilterDataProvider<EntityClass, Void, String>) getGrid()
                .getDataProvider()).setFilter(valueChanged.getValue());
        });
        return tf;
    }
}
