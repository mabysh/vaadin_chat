package com.gpsolutions.vaadin_course2.vaadin.views;

import com.gpsolutions.vaadin_course2.model.Message;
import com.gpsolutions.vaadin_course2.vaadin.ChatUI;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

@SpringView(name = MessagesListView.NAME, ui = ChatUI.class)
@Component(MessagesListView.NAME)
@Secured("ADMIN")
public class MessagesListView extends AbstractEntityListView<Message> {

    public static final String NAME = "messages";

    @Override
    protected Class<Message> getEntityClass() {
        return Message.class;
    }
}


