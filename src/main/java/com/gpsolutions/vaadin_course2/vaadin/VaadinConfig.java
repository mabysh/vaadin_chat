package com.gpsolutions.vaadin_course2.vaadin;

import com.gpsolutions.vaadin_course2.model.AbstractEntity;
import com.gpsolutions.vaadin_course2.vaadin.annotations.CustomRenderer;
import com.gpsolutions.vaadin_course2.vaadin.annotations.Position;
import com.vaadin.navigator.Navigator;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.spring.navigator.SpringNavigator;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.Renderer;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ReflectionUtils;

@Configuration
public class VaadinConfig {

    @Bean
    @ViewScope
    public <EntityClass extends AbstractEntity> Grid<EntityClass> getGrid(
        final DependencyDescriptor descriptor) {
        final Class<?> type = descriptor.getResolvableType().getGeneric(0).resolve();
        Grid<EntityClass> grid = new Grid<>();
        grid.setSizeFull();
        grid.setSelectionMode(SelectionMode.MULTI);
        grid.setCaption(type.getSimpleName() + " list");
        setupColumns(grid, type);
        return grid;
    }

    private <EntityClass> void setupColumns(final Grid<EntityClass> grid, Class<?> entityClass) {
        List<Field> fields = new ArrayList<>();
        ReflectionUtils.doWithFields(entityClass, fields::add);
        fields.stream().sorted(Comparator.comparing(this::retrievePosition)).forEach(field -> {
            if (Iterable.class.isAssignableFrom(field.getType())) {
                return;
            }
            Column<EntityClass, ?> column = grid.addColumn(entity -> {
                try {
                    if (!field.isAccessible()) {
                        field.setAccessible(true);
                    }
                    return field.get(entity);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("Failed to retrieve property value", e);
                }
            });
            column.setMaximumWidth(400);
            setColumnCaption(column, field.getName());
            setColumnRenderer(column, field);
        });

    }

    private <EntityClass> void setColumnCaption(final Column<EntityClass, ?> column,
        final String fieldName) {
        final String columnCaption = StringUtils.capitalize(StringUtils
            .join(StringUtils.splitByCharacterTypeCamelCase(fieldName), StringUtils.SPACE));
        column.setCaption(columnCaption);
    }

    private <EntityClass> void setColumnRenderer(final Column<EntityClass, ?> column,
        final Field field) {
        final CustomRenderer customRenderer = field.getAnnotation(CustomRenderer.class);
        if (customRenderer != null) {
            try {
                final Renderer renderer = customRenderer.value().newInstance();
                column.setRenderer(renderer);
            } catch (InstantiationException | IllegalAccessException e) {
                throw new RuntimeException("Failed to instantiate renderer", e);
            }
        }
    }

    private Integer retrievePosition(final Field f1) {
        final Position a1 = f1.getAnnotation(Position.class);
        return a1 == null ? Integer.MAX_VALUE : a1.value();
    }
}
