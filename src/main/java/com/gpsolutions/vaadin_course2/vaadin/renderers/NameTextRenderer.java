package com.gpsolutions.vaadin_course2.vaadin.renderers;

import com.gpsolutions.vaadin_course2.model.AbstractNamedEntity;
import com.vaadin.ui.renderers.TextRenderer;
import elemental.json.Json;
import elemental.json.JsonValue;

public class NameTextRenderer extends TextRenderer {

    @Override
    public JsonValue encode(Object value) {
        if (value == null) {
            return super.encode(null);
        } else if (value instanceof AbstractNamedEntity) {
            return Json.create(((AbstractNamedEntity) value).getName());
        } else {
            return Json.create(value.toString());
        }
    }
}
