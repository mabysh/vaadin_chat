package com.gpsolutions.vaadin_course2.vaadin.renderers;

import com.gpsolutions.vaadin_course2.model.AbstractNamedEntity;
import com.vaadin.ui.renderers.TextRenderer;
import elemental.json.Json;
import elemental.json.JsonValue;

public class PasswordRenderer extends TextRenderer {

    @Override
    public JsonValue encode(Object value) {
        if (value == null) {
            return super.encode(null);
        } else {
            return Json.create(encodePass(value.toString()));
        }
    }

    private String encodePass(final String pass){

        return ((String) pass).replaceAll(".","*");
    }
}
