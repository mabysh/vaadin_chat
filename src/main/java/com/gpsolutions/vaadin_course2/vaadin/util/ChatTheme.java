package com.gpsolutions.vaadin_course2.vaadin.util;

public class ChatTheme {

    public static final String TEXT_ALIGN_LEFT = "text-align-left";
    public static final String WORD_WRAP = "wordwrap";
    public static final String MARGIN_BOTTOM_15 = "margin-bottom-15";
    public static final String SCROLLABLE = "scrollable";

    private ChatTheme() {
    }
}
