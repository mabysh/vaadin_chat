package com.gpsolutions.vaadin_course2.service;

import com.gpsolutions.vaadin_course2.dao.MessageRepository;
import com.gpsolutions.vaadin_course2.model.Message;
import com.gpsolutions.vaadin_course2.model.Room;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MessageService extends AbstractService<Message> {

    @Transactional
    public Map<String, List<Message>> buildMessagesByRoomsMap(final List<Room> rooms) {
        Map<String, List<Message>> map = new ConcurrentHashMap<>();
        rooms.forEach(room -> {
            final List<Message> messagesForRoom = findMessagesForRoom(room);
            map.put(room.getName(), messagesForRoom);
        });
        return map;
    }

    private List<Message> findMessagesForRoom(final Room room) {
        return getRepository().findByRoom(room);
    }

    @Override
    public MessageRepository getRepository() {
        return (MessageRepository) super.getRepository();
    }
}
