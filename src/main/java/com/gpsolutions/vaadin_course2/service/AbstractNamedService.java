package com.gpsolutions.vaadin_course2.service;

import com.gpsolutions.vaadin_course2.dao.NamedEntityRepository;
import com.gpsolutions.vaadin_course2.model.AbstractNamedEntity;
import com.vaadin.data.provider.Query;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.data.domain.PageRequest;

public abstract class AbstractNamedService<EntityClass extends AbstractNamedEntity> extends
    AbstractService<EntityClass> {


    @Override
    public NamedEntityRepository<EntityClass> getRepository() {
        return (NamedEntityRepository<EntityClass>) super.getRepository();
    }

    public Stream<EntityClass> findFilteredByNameWithPagination(
        final Query<EntityClass, String> query) {
        PageRequest pageRequest = preparePageRequest(query);
        List<EntityClass> items = null;
        if (query.getFilter().isPresent()) {
            items = getRepository().findByNameContaining(query.getFilter().get(), pageRequest)
                .getContent();
        } else {
            items = getRepository().findAll(pageRequest).getContent();
        }
        return items.stream();
        // TODO research about this comment
//        return items.subList(query.getOffset() % query.getLimit(), items.size())
//            .stream();
    }

    public Optional<EntityClass> findByName(final String username) {
        return getRepository().findByName(username);
    }
}

