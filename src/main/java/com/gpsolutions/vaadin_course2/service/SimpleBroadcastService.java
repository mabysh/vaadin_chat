package com.gpsolutions.vaadin_course2.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.stereotype.Service;

@Service
public class SimpleBroadcastService<MessageType> implements BroadcastService<MessageType> {

    private final ExecutorService executorService;
    private final Map<String, BroadcastListener<MessageType>> listenerMap;

    public SimpleBroadcastService() {
        this.executorService = Executors.newSingleThreadExecutor();
        this.listenerMap = new ConcurrentHashMap<>();
    }

    @Override
    public void register(final BroadcastListener<MessageType> listener) {
        listenerMap.put(listener.getIdentifier(), listener);
    }

    @Override
    public void unregister(final BroadcastListener<MessageType> listener) {
        listenerMap.remove(listener.getIdentifier());
    }

    @Override
    public void broadcast(final MessageType message) {
        for (BroadcastListener listener : listenerMap.values()) {
            executorService.execute(() -> listener.onMessage(message));
        }
    }
}
