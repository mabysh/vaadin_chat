package com.gpsolutions.vaadin_course2.service;

import com.gpsolutions.vaadin_course2.model.Room;
import com.gpsolutions.vaadin_course2.model.User;
import java.util.List;
import java.util.Optional;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service
public class UserService extends AbstractNamedService<User> {

    @Transactional
    public List<Room> findCurrentUserRooms() {
        return getCurrentUser().getRooms();
    }

    @Transactional
    public User getCurrentUser() {
        final Authentication authentication = SecurityContextHolder.getContext()
            .getAuthentication();
        final String name = authentication.getName();
        final Optional<User> byId = getRepository().findByName(name);
        Assert.isTrue(byId.isPresent(), "No user with name: '" + name + "'");
        return byId.get();
    }
}
