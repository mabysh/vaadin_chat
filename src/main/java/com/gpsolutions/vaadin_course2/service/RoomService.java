package com.gpsolutions.vaadin_course2.service;

import com.gpsolutions.vaadin_course2.model.Room;
import org.springframework.stereotype.Service;

@Service
public class RoomService extends AbstractNamedService<Room> {

}
