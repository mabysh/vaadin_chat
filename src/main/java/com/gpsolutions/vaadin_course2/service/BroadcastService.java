package com.gpsolutions.vaadin_course2.service;

public interface BroadcastService<MessageType> {

    void register(final BroadcastListener<MessageType> listener);

    void unregister(final BroadcastListener<MessageType> listener);

    void broadcast(final MessageType message);
}
