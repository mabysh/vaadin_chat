package com.gpsolutions.vaadin_course2.service;

import com.gpsolutions.vaadin_course2.model.UserCredentials;

public interface AccessControlService {

    boolean hasAccess(final UserCredentials userCredentials);
}
