package com.gpsolutions.vaadin_course2.service;

public interface BroadcastListener<MessageType> {

    String getIdentifier();

    void onMessage(final MessageType message);
}
