package com.gpsolutions.vaadin_course2.service;

import com.gpsolutions.vaadin_course2.model.AbstractEntity;
import com.vaadin.data.provider.Query;
import com.vaadin.shared.data.sort.SortDirection;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public class AbstractService<EntityClass extends AbstractEntity> {

    @Autowired
    private JpaRepository<EntityClass, Long> repository;

    public JpaRepository<EntityClass, Long> getRepository() {
        return repository;
    }

    public List<EntityClass> findAll() {
        return getRepository().findAll();
    }

    public Stream<EntityClass> findWithPagination(
        final Query<EntityClass, String> query) {
        final PageRequest pageRequest = preparePageRequest(query);
        final List<EntityClass> items = getRepository().findAll(pageRequest).getContent();
        return items.stream();
        // TODO research about this comment
//        return items.subList(query.getOffset() % query.getLimit(), items.size())
//            .stream();
    }

    protected PageRequest preparePageRequest(final Query<EntityClass, String> query) {
        final int page = query.getOffset() / query.getLimit();

        final List<Order> sortOrders = query.getSortOrders().stream()
            .map(sortOrder -> new Sort.Order(
                sortOrder.getDirection() == SortDirection.ASCENDING ? Sort.Direction.ASC
                    : Sort.Direction.DESC,
                sortOrder.getSorted()))
            .collect(Collectors.toList());

        return PageRequest.of(page, query.getLimit(),
            sortOrders.isEmpty() ? Sort.unsorted() : Sort.by(sortOrders));
    }

    @Transactional
    public EntityClass save(final EntityClass toSave) {
        toSave.beforeSave();
        return getRepository().save(toSave);
    }

    public void deleteAll(final Collection<EntityClass> toDelete) {
        getRepository().deleteAll(toDelete);
    }

    public long count() {
        return getRepository().count();
    }

    public Optional<EntityClass> findById(final Long id) {
        return getRepository().findById(id);
    }
}
