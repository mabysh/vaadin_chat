package com.gpsolutions.vaadin_course2.dao;

import com.gpsolutions.vaadin_course2.model.AbstractNamedEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface NamedEntityRepository<EntityClass extends AbstractNamedEntity> extends
    JpaRepository<EntityClass, Long> {

    List<EntityClass> findByNameContaining(final String namePart);

    Page<EntityClass> findByNameContaining(final String namePart, final Pageable pageable);

    Optional<EntityClass> findByName(final String name);
}
