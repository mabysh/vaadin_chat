package com.gpsolutions.vaadin_course2.dao;

import com.gpsolutions.vaadin_course2.model.Message;
import com.gpsolutions.vaadin_course2.model.Room;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findByRoom(final Room room);
}
