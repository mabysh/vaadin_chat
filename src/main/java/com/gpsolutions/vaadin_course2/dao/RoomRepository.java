package com.gpsolutions.vaadin_course2.dao;

import com.gpsolutions.vaadin_course2.model.Room;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends NamedEntityRepository<Room> {

    List<Room> findByNameContaining(final String namePart);

    Page<Room> findByNameContaining(final String namePart, final Pageable pageable);

//    long countByNameContaining(final String namePart);
//
//    long countByNameContaining(final String namePart, final Pageable pageable);
//
//    long count(final Pageable pageable);

    Optional<Room> findByName(final String name);
}
